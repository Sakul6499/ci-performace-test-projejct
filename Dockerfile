ARG path=/home/gradle/src

# -----------------------------------------------

FROM gradle:jdk8 as builder
USER root

# just copy everything
COPY --chown=gradle:gradle ./ $path/
WORKDIR $path/

RUN gradle build --scan

# -----------------------------------------------

FROM openjdk:8-jre

COPY --from=builder $path/build/libs/ /app/
WORKDIR /app

CMD ["java", "-server", "-XX:+UnlockExperimentalVMOptions", "-XX:+UseCGroupMemoryLimitForHeap", "-XX:InitialRAMFraction=2", "-XX:MinRAMFraction=2", "-XX:MaxRAMFraction=2", "-XX:+UseG1GC", "-XX:MaxGCPauseMillis=100", "-XX:+UseStringDeduplication", "-jar", "Android-Authentication-Server.jar"]
