package de.cow_gaming.android.server.v1

import com.google.gson.*
import org.slf4j.LoggerFactory
import java.io.File
import java.lang.reflect.Type
import java.util.*
import kotlin.concurrent.timerTask

@Deprecated("Use v2 instead!")
class AuthProvider {

    //private val authInfo = mutableMapOf<String, String>()
    private var authInfo = listOf<AuthObject>()
    private val lock =  Object()
    private val logger = LoggerFactory.getLogger(AuthProvider::class.java)

    private val timerDelay: Long = 0 // exec immediately
    private val timerPeriod: Long = 1000 * 60 * 15 // every 15 min

    init {
        // reload authInfo every [timerPeriod]ms from FileSystem
        Timer().scheduleAtFixedRate(timerTask {
            readFile()
        }, timerDelay, timerPeriod)
    }

    fun getPrincipal(name: String, password: String): AuthPrincipal? {
        synchronized(lock){
            return authInfo.singleOrNull { it.username == name && it.password == password }?.toPrincipal()
        }
    }

    fun isValid(principal: AuthPrincipal): Boolean {
        synchronized(lock){
            return authInfo.any { it.username == principal.username && it.email == principal.email }
        }
    }

    //-------------------------------------------

    private val authFileName = "authentication.json"
    private val confFolderName = "conf"

    private fun readFile(){
        val confFolder = File(confFolderName)
        confFolder.mkdirs()

        val authFile = File(confFolder, authFileName)

        // only creates if it does not exist
        authFile.createNewFile()

        val builder = GsonBuilder()
        builder.registerTypeAdapter(AuthObject::class.java, AuthObjectDeserializer())
        val gson = builder.create()

        val jsonReader = authFile.reader(charset = Charsets.UTF_8)
        val authObjectList =
            try {
                gson.fromJson(jsonReader, Array<AuthObject>::class.java).toList()
            } catch (e: IllegalStateException){
                null
            }

        if(!authObjectList.isNullOrEmpty()){
            synchronized(lock){
                authInfo = authObjectList!!.filter { it.isValid() }
            }
            logger.info("AuthInfo has been updated!")
            //authInfo.forEach { println(it) }
        }
    }

}

@Deprecated("Use v2 instead!")
private data class AuthObject(
    val username: String,
    val password: String,
    val email: String,
    val permissions: Collection<String> = emptyList()
) {
    fun isValid(): Boolean = username.isNotBlank() && password.isNotBlank() && email.isNotBlank()
    fun toPrincipal(): AuthPrincipal = AuthPrincipal(username, email, permissions)
}

@Deprecated("Use v2 instead!")
private class AuthObjectDeserializer: JsonDeserializer<AuthObject> {

    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): AuthObject {
        if(json == null) return AuthObject("", "", "", emptySet())

        val obj = json as JsonObject
        return AuthObject(
            username = obj.get("username").asString,
            password = obj["password"].asString,
            email = obj["email"].asString,
            permissions = obj["permissions"].asJsonArray.map { it.asString }.toList()
        )
    }

}