package de.cow_gaming.android.server.v1

import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.messaging.Message
import com.google.firebase.messaging.MulticastMessage
import com.google.firebase.messaging.Notification
import de.cow_gaming.android.server.FirebaseInstance
import org.slf4j.LoggerFactory

@Deprecated("Use v2 instead!")
class FcmClient {

    //private val app: FirebaseApp
    private val messaging: FirebaseMessaging
    private val fileName = "cow-gaming-fcm-key.json"

    private val logger = LoggerFactory.getLogger(FcmClient::class.java)

    init {
        /*val sAF = File("resources", fileName)
        val serviceAccount = if(sAF.exists()) FileInputStream(sAF) else javaClass.classLoader.getResourceAsStream(fileName)

        val options = FirebaseOptions.Builder()
            .setCredentials(GoogleCredentials.fromStream(serviceAccount))
            .setDatabaseUrl("https://cow-gaming-1582912575083.firebaseio.com")
            .build()

        app = FirebaseApp.initializeApp(options)
        messaging = FirebaseMessaging.getInstance(app)*/
        messaging = FirebaseInstance.getMessaging()
    }

    //-------------------------------------------

    @Throws(Exception::class)
    fun sendNotification(notification: Notification){

    }

    @Throws(Exception::class)
    fun sendMessage(message: Message){
        val response = messaging.send(message)
        logger.info("Message Response: $response")
    }

    @Throws(Exception::class)
    fun sendMulticastMessage(message: MulticastMessage){
        val response = messaging.sendMulticast(message)

        logger.info("MulticastMessage Response: success=${response.successCount}, failed=${response.failureCount}")
        response.responses.forEach {
            logger.info("Message Response: ${if(it.isSuccessful) it.messageId else it.exception.message}")
        }
    }
}