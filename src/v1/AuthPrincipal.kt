package de.cow_gaming.android.server.v1

import io.ktor.auth.Principal

@Deprecated("Use v2 instead!")
data class AuthPrincipal (
    val username: String,
    val email: String,
    val permissions: Collection<String> = emptySet()
): Principal