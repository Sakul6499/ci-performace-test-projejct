package de.cow_gaming.android.server.v1

import org.slf4j.LoggerFactory
import java.util.*
import kotlin.concurrent.timerTask

@Deprecated("Use v2 instead!")
class TokenStore(private val authProvider: AuthProvider) {
    private var store: MutableMap<AuthPrincipal, MutableSet<String>> = mutableMapOf()
    private val lock =  Object()
    private val logger = LoggerFactory.getLogger(TokenStore::class.java)

    private val timerDelay: Long = 0 // exec immediately
    private val timerPeriod: Long = 1000 * 60 * 15 // every 15 min

    init {
        // reload authInfo every [timerPeriod]ms from FileSystem
        Timer().scheduleAtFixedRate(timerTask {
            cleanup()
        }, timerDelay, timerPeriod)
    }

    //-------------------------------------------

    fun add(principal: AuthPrincipal, fcmToken: String){
        synchronized(lock){
            if(!store.containsKey(principal)) store[principal] = mutableSetOf()
            store[principal]?.add(fcmToken)
        }
        logger.info("Added FcmToken $fcmToken to principal $principal")
    }

    fun remove(principal: AuthPrincipal, fcmToken: String){
        synchronized(lock){
            store[principal]?.remove(fcmToken)
        }
        logger.info("Removed FcmToken $fcmToken to principal $principal")
    }

    fun getAllTokens(): Collection<String> {
        val tokens = mutableSetOf<String>()
        synchronized(lock){
            store.values.forEach { tokens.addAll(it) }
        }
        return tokens
    }

    fun invalidateToken(fcmToken: String){
        synchronized(lock){
            store.values.forEach {
                if(it.contains(fcmToken)) it.remove(fcmToken)
            }
        }
        logger.info("Invalidated Token $fcmToken")
    }

    //-------------------------------------------

    private fun cleanup(){
        synchronized(lock){
            store = store.filter { authProvider.isValid(it.key) }.toMutableMap()
        }
    }

    //-------------------------------------------

    companion object {
        const val POST_FIELD = "fcmtoken"
    }
}