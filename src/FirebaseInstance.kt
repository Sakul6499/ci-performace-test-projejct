package de.cow_gaming.android.server

import com.google.auth.oauth2.GoogleCredentials
import com.google.firebase.FirebaseApp
import com.google.firebase.FirebaseOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.messaging.FirebaseMessaging
import java.io.File
import java.io.FileInputStream

object FirebaseInstance {

    private val fileName = "cow-gaming-fcm-key.json"
    private var app: FirebaseApp? = null

    private fun initApp() {
        if(app == null){
            val sAF = File("resources", fileName)
            val serviceAccount = if(sAF.exists()) FileInputStream(sAF) else javaClass.classLoader.getResourceAsStream(fileName)

            val options = FirebaseOptions.Builder()
                .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                .setDatabaseUrl("https://cow-gaming-1582912575083.firebaseio.com")
                .build()

            app = FirebaseApp.initializeApp(options)
        }
    }

    //-------------------------------------------

    fun getAuth(): FirebaseAuth {
        initApp()
        return FirebaseAuth.getInstance(app!!)
    }

    fun getMessaging(): FirebaseMessaging {
        initApp()
        return FirebaseMessaging.getInstance(app)
    }

}