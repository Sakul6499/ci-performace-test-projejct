package de.cow_gaming.android.server

import com.google.gson.Gson
import de.cow_gaming.android.server.v2.AuthProvider
import de.cow_gaming.android.server.v2.FcmClient
import de.cow_gaming.android.server.v2.TokenStore
import de.cow_gaming.android.server.v2.ts3.TS3Bot
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.auth.*
import io.ktor.features.*
import io.ktor.gson.gson
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.Locations
import io.ktor.request.path
import io.ktor.request.receiveParameters
import io.ktor.response.respond
import io.ktor.response.respondText
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.route
import io.ktor.routing.routing
import org.slf4j.event.Level
import java.text.DateFormat
import kotlin.concurrent.thread

const val AUTH_REALM = "CoW-Realm"

val authProvider = AuthProvider()
val tokenStore = TokenStore(authProvider)
val fcmClient = FcmClient(tokenStore)
val ts3Bot = TS3Bot(fcmClient)

fun main(args: Array<String>) {
    Runtime.getRuntime().addShutdownHook(thread {
        ts3Bot.shutdown()
        tokenStore.store()
    })
    return io.ktor.server.netty.EngineMain.main(args)
}

@KtorExperimentalLocationsAPI
@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.module(testing: Boolean = false) {
    install(Locations) { }

    /*install(Sessions) {
        cookie<MySession>("MY_SESSION") {
            cookie.extensions["SameSite"] = "lax"
        }
    }*/

    install(Compression) {
        gzip {
            priority = 1.0
        }
        deflate {
            priority = 10.0
            minimumSize(1024) // condition
        }
    }

    install(CallLogging) {
        level = Level.INFO
        filter { call -> call.request.path().startsWith("/") }
    }

    install(ForwardedHeaderSupport) // WARNING: for security, do not include this if not behind a reverse proxy
    install(XForwardedHeaderSupport) // WARNING: for security, do not include this if not behind a reverse proxy

    install(ContentNegotiation){
        gson {
            setDateFormat(DateFormat.LONG)
            setPrettyPrinting()
        }
    }

    install(Authentication) {
        basic(name = AUTH_REALM) {
            realm = AUTH_REALM
            validate { authProvider.validate(it.name, it.password) }
        }
    }

    routing {
        authenticate(AUTH_REALM) {
            route("/v2"){
                route("/token"){
                    post("/add"){
                        val params = call.receiveParameters()
                        val principal = call.principal<UserIdPrincipal>()!!

                        if(params.contains(TokenStore.POST_FIELD)){
                            tokenStore.add(principal, params[TokenStore.POST_FIELD]!!)
                            call.respond(HttpStatusCode.OK)
                        } else call.respond(HttpStatusCode.BadRequest)
                    }
                    post("/remove"){
                        val params = call.receiveParameters()
                        val principal = call.principal<UserIdPrincipal>()!!

                        if(params.contains(TokenStore.POST_FIELD)){
                            tokenStore.remove(principal, params[TokenStore.POST_FIELD]!!)
                            call.respond(HttpStatusCode.OK)
                        } else call.respond(HttpStatusCode.BadRequest)
                    }
                }
                get("permissions"){
                    val principal = call.principal<UserIdPrincipal>()
                    val perms = authProvider.getUserPerms(principal!!)
                    val json = Gson().toJson(perms)

                    call.respondText(contentType = ContentType.Application.Json, text = json, status = HttpStatusCode.OK)
                }
            }
        }
    }
}