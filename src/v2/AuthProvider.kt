package de.cow_gaming.android.server.v2

import de.cow_gaming.android.server.FirebaseInstance
import io.ktor.auth.UserIdPrincipal
import org.slf4j.LoggerFactory
import java.util.*
import kotlin.concurrent.timerTask

class AuthProvider {

    private val lock = Object()
    private val logger = LoggerFactory.getLogger(AuthProvider::class.java)
    private val permissionFileName = "permissions.json"
    private var cache: MutableMap<String, MutableSet<String>> = mutableMapOf()

    init {
        Timer().scheduleAtFixedRate(timerTask {
            reloadPerms()
        }, TimerParams.DELAY_AUTH, TimerParams.PERIOD)
    }

    //-------------------------------------------

    /**
     * Validates if the selected user exists in the local database<br>
     *     throws FirebaseAuthException if [email] does not exist
     */
    fun validate(email: String, uid: String): UserIdPrincipal? {
        return try {
            val user = FirebaseInstance.getAuth().getUserByEmail(email)
            if(user.uid == uid) {
                if(!cache.containsKey(user.uid)){
                    val set = mutableSetOf<String>()
                    UserFile.storeSingleUserData(user.uid, permissionFileName, set)
                    cache[user.uid] = set
                }
                UserIdPrincipal(user.uid)
            } else null
        } catch (e: Exception){
            null
        }
    }

    fun getUserPerms(principal: UserIdPrincipal): Set<String> {
        synchronized(lock){
            return cache[principal.name] ?: emptySet()
        }
    }

    fun hasPermission(principal: UserIdPrincipal, permission: Permission): Boolean = hasPermission(principal.name, permission)

    fun hasPermission(userID: String, permission: Permission): Boolean {
        synchronized(lock){
            return cache[userID]?.contains(permission.permission) ?: false
        }
    }

    //-------------------------------------------

    fun reloadPerms(){
        logger.info("Reloading...")
        synchronized(lock){
            cache = UserFile.readAllUserData(permissionFileName)
        }
        logger.info("Reload complete!")
    }

}