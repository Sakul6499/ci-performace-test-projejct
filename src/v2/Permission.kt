package de.cow_gaming.android.server.v2

enum class Permission(val permission: String) {

    NOTIFY_SUPPORT("notification.support"),
    NOTIFY_GENERAL("notification.general"),
    NOTIFY_ADMIN("notification.admin"),

    GROUP_MEMBER("group.member"),
    GROUP_SUPPORT("group.support"),
    GROUP_ADMIN("group.admin")

}