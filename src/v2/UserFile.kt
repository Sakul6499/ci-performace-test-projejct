package de.cow_gaming.android.server.v2

import com.google.gson.Gson
import io.ktor.auth.UserIdPrincipal
import java.io.File

object UserFile {

    private val lock = Object()
    private val userDataFolderName = "conf${File.separator}users"

    fun readSingleUserData(principal: UserIdPrincipal, fileName: String): MutableSet<String> {
        val userDataFolder = File(userDataFolderName, principal.name)
        userDataFolder.mkdirs()

        userDataFolder.listFiles()?.singleOrNull { it.name == principal.name && it.isDirectory }?.let { userFolder ->
            val dataFile = File(userFolder, fileName)
            // only creates if it does not exist
            dataFile.createNewFile()

            return try {
                synchronized(lock){
                    Gson().fromJson(dataFile.bufferedReader(charset = Charsets.UTF_8), Array<String>::class.java)
                }.toMutableSet()
            } catch (e: Exception) {
                mutableSetOf()
            }
        }

        return mutableSetOf()
    }

    fun readAllUserData(fileName: String): MutableMap<String, MutableSet<String>> {
        val map: MutableMap<String, MutableSet<String>> = mutableMapOf()

        val userDataFolder = File(userDataFolderName)
        userDataFolder.mkdirs()

        synchronized(lock){
            userDataFolder.listFiles()?.filter { it.isDirectory }?.forEach { userFolder ->
                val dataFile = File(userFolder, fileName)
                // only creates if it does not exist
                dataFile.createNewFile()

                map[userFolder.name] = try {
                    Gson().fromJson(dataFile.bufferedReader(charset = Charsets.UTF_8), Array<String>::class.java).toMutableSet()
                } catch (e: Exception) {
                    mutableSetOf<String>()
                }
            }
        }

        return map
    }

    //-------------------------------------------

    fun storeSingleUserData(userID: String, fileName: String, data: Set<String>){
        val userDataFolder = File(userDataFolderName, userID)
        userDataFolder.mkdirs()

        val jsonData = Gson().toJson(data.toTypedArray())
        synchronized(lock){
            val writeFile = File(userDataFolder, fileName)
            writeFile.createNewFile()
            writeFile.writeText(jsonData, charset = Charsets.UTF_8)
        }
    }

}