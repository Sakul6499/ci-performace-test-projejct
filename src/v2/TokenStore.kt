package de.cow_gaming.android.server.v2

import io.ktor.auth.UserIdPrincipal
import org.slf4j.LoggerFactory
import java.util.*
import kotlin.concurrent.timerTask

class TokenStore(private val authProvider: AuthProvider) {

    companion object {
        const val POST_FIELD = "fcmtoken"
    }

    private val lock = Object()
    private val logger = LoggerFactory.getLogger(TokenStore::class.java)
    private var cache: MutableMap<String, MutableSet<String>> = mutableMapOf()
    private val tokenFileName = "tokens.json"

    init {
        load()

        Timer().scheduleAtFixedRate(timerTask {
            store()
        }, TimerParams.DELAY_TOKEN, TimerParams.PERIOD)
    }

    //-------------------------------------------

    fun add(principal: UserIdPrincipal, fcmToken: String){
        synchronized(lock){
            if(!cache.containsKey(principal.name)) {
                cache[principal.name] = mutableSetOf()
            }
            cache[principal.name]!!.add(fcmToken)
        }
        logger.info("Added Token <$fcmToken> for <${principal.name}>")
    }

    fun remove(principal: UserIdPrincipal, fcmToken: String){
        synchronized(lock){
            if(cache.containsKey(principal.name)){
                cache[principal.name]?.remove(fcmToken)
            }
        }
        logger.info("Removed Token <$fcmToken> for <${principal.name}>")
    }

    fun invalidate(fcmToken: String){
        synchronized(lock){
            cache.values.forEach { it.remove(fcmToken) }
        }
        logger.info("Invalidated Token <$fcmToken>")
    }

    //-------------------------------------------

    fun getAllTokens(): Set<String> {
        val tokens = mutableSetOf<String>()
        synchronized(lock){
            cache.values.forEach { tokens.addAll(it) }
        }
        return tokens
    }

    fun getAllTokensPermission(permission: Permission): Set<String> {
        val tokens = mutableSetOf<String>()
        synchronized(lock){
            cache.forEach { (k, v) ->
                if(authProvider.hasPermission(k, permission)) {
                    tokens.addAll(v)
                }
            }
        }
        return tokens
    }

    //-------------------------------------------

    fun store(){
        logger.info("Writing Tokens to disk...")
        synchronized(lock){
            cache.forEach { (userID, tokens) ->
                UserFile.storeSingleUserData(userID, tokenFileName, tokens)
            }
        }
        logger.info("Write complete")
    }

    private fun load(){
        logger.info("Loading Tokens from disk")
        synchronized(lock){
            cache = UserFile.readAllUserData(tokenFileName)
        }
        logger.info("Load complete")
    }

}