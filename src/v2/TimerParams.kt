package de.cow_gaming.android.server.v2

object TimerParams {

    const val DELAY_TOKEN: Long = 1000 * 60 * 15 // exec after 15 min
    const val DELAY_AUTH: Long = 0 // exec immediately

    const val PERIOD: Long = 1000 * 60 * 15 // every 15 min

}