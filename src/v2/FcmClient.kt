package de.cow_gaming.android.server.v2

import com.google.firebase.messaging.MulticastMessage
import com.google.firebase.messaging.Notification
import de.cow_gaming.android.server.FirebaseInstance
import de.cow_gaming.android.server.v1.FcmClient
import org.slf4j.LoggerFactory

class FcmClient(private val tokenStore: TokenStore) {

    private val logger = LoggerFactory.getLogger(FcmClient::class.java)

    //-------------------------------------------

    fun sendNotification(title: String, body: String){
        sendNotificationInternal(title, body, tokenStore.getAllTokens())
    }

    fun sendNotificationPermission(title: String, body: String, permission: Permission){
        sendNotificationInternal(title, body, tokenStore.getAllTokensPermission(permission))
    }

    //-------------------------------------------

    private fun sendNotificationInternal(title: String, body: String, tokens: Set<String>){
        if(tokens.isEmpty()) return

        try {
            val notification = Notification.builder()
                .setTitle(title)
                .setBody(body)
                .build()

            val message = MulticastMessage.builder()
                .setNotification(notification)
                .addAllTokens(tokens)
                .build()

            FirebaseInstance.getMessaging().sendMulticast(message)
        } catch (e: Exception){
            logger.error("Unknown Error", e)
        }
    }

}