package de.cow_gaming.android.server.v2.ts3

import com.github.theholywaffle.teamspeak3.TS3Query
import com.github.theholywaffle.teamspeak3.api.reconnect.ConnectionHandler
import de.cow_gaming.android.server.v2.FcmClient
import de.cow_gaming.android.server.v2.Permission

class ConnectionAdapter(private val fcmClient: FcmClient) : ConnectionHandler {

    override fun onConnect(ts3Query: TS3Query?) {
        if(ts3Query == null) return
        else login(ts3Query)
    }

    override fun onDisconnect(ts3Query: TS3Query?) {
        fcmClient.sendNotificationPermission("TS3Bot", "Bot is offline", Permission.NOTIFY_ADMIN)
    }

    //-------------------------------------------

    private fun login(ts3Query: TS3Query){
        val api = ts3Query.api
        api.login("CaluconAndroid", "jmcDWYwi")
        api.selectVirtualServerByPort(9030)
        try { api.setNickname("CaluconAndroid") } catch (e: Exception) { }

        api.registerAllEvents()
        api.addTS3Listeners(Listener(api, fcmClient))

        fcmClient.sendNotificationPermission("TS3Bot", "Bot is online", Permission.NOTIFY_ADMIN)
    }

}