package de.cow_gaming.android.server.v2.ts3

import com.github.theholywaffle.teamspeak3.TS3Api
import com.github.theholywaffle.teamspeak3.api.event.*
import de.cow_gaming.android.server.v2.FcmClient
import de.cow_gaming.android.server.v2.Permission

class Listener(private val api: TS3Api, private val fcmClient: FcmClient): TS3Listener {

    companion object {
        const val CHANNEL_SUPP_WAIT: Int = 333838
    }

    //-------------------------------------------

    override fun onClientJoin(e: ClientJoinEvent?) {

    }

    override fun onClientLeave(e: ClientLeaveEvent?) {

    }

    override fun onClientMoved(e: ClientMovedEvent?) {
        if(e == null) return

        if(e.targetChannelId == CHANNEL_SUPP_WAIT){
            api.clients.singleOrNull { it.id == e.clientId }?.let {
                fcmClient.sendNotificationPermission("Support", "${it.nickname} ist im Warteraum", Permission.NOTIFY_SUPPORT)
            }
        }
    }

    //-------------------------------------------

    override fun onServerEdit(e: ServerEditedEvent?) {

    }

    override fun onChannelCreate(e: ChannelCreateEvent?) {

    }

    override fun onChannelPasswordChanged(e: ChannelPasswordChangedEvent?) {

    }

    override fun onChannelDeleted(e: ChannelDeletedEvent?) {

    }

    override fun onChannelMoved(e: ChannelMovedEvent?) {

    }

    override fun onPrivilegeKeyUsed(e: PrivilegeKeyUsedEvent?) {

    }

    override fun onChannelEdit(e: ChannelEditedEvent?) {

    }

    override fun onChannelDescriptionChanged(e: ChannelDescriptionEditedEvent?) {

    }

    //-------------------------------------------

    override fun onTextMessage(e: TextMessageEvent?) {
        if(e == null) return

        fcmClient.sendNotificationPermission("TextMessage", "${e.invokerName} >> ${e.message}", Permission.NOTIFY_ADMIN)
    }

}