package de.cow_gaming.android.server.v2.ts3

import com.github.theholywaffle.teamspeak3.TS3Config
import com.github.theholywaffle.teamspeak3.TS3Query
import com.github.theholywaffle.teamspeak3.api.reconnect.ReconnectStrategy
import de.cow_gaming.android.server.v2.FcmClient

class TS3Bot(private val fcmClient: FcmClient) {

    private val query: TS3Query

    init {
        val config = TS3Config().apply {
            setHost("79.133.54.221")
            setQueryPort(10011)
            //setEnableCommunicationsLogging(true)
            setReconnectStrategy(ReconnectStrategy.constantBackoff())
            setConnectionHandler(ConnectionAdapter(fcmClient))
        }

        query = TS3Query(config)
        query.connect()

        // ConnectionAdapter manages login etc
    }

    fun shutdown(){
        query.exit()
    }

}
